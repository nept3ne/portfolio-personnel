class documentHelper{

    constructor(){}

    getAll(idParent ){
        //idParent -> This is the html id of the parent
        //send back all the child of the parent
        var containerParent = document.getElementById(idParent); // I try to get the parent node from it's id
        if (containerParent==null){containerParent=document.getElementsByClassName(idParent)[0];}
        if (containerParent==null){containerParent=document.getElementsByTagName(idParent)[0];}
        if (containerParent!=null){ // we look if the item search exist
            var Allchild = containerParent.childNodes; //we took all child
            var arrayChild=[]; // we create an array for all the new child
            Allchild.forEach(child => { // we take all child one per one
                if (child.tagName!=null){ // we check if the child have a tagname, somtimes it happens that not, and we don't want thes
                    arrayChild = arrayChild.concat(child); // we add all the correct child to the array precedently created
                }
            });
            return arrayChild; // we sendback all the correct child
        }else{
            return null; // otherwise we return nothing
        }
    }

    getAllExct(idParent,tag ){
        //idParent -> This is the html id of the parent
        // tag -> This is all html tag to exclude
        //it sendback all the child except which have the specified tag
        var containerParent = document.getElementById(idParent); // we took an html element by its id
        var Allchild = containerParent.childNodes; // we take all of its child
        var arrayChild=[]; // we create an array for the child
        Allchild.forEach(child => { // for each child
            if (child.tagName!=null && child.tagName!=tag){ // if it's tag name is not null and his tag name is not equals to the one specified above 
                arrayChild = arrayChild.concat(child); // we add the child to the array
            }
        });
        return arrayChild   ; // we return the array
    }

    filter(idParent,classN){
        // idParent -> This is the html id of the parent
        // classN ->This is the name of the class we don't want
        // it return -> nothing
        // it remove for each child of the parent a class (here it is specified by classN)
        var Allchilds = this.getAll(idParent); // I take all of childs
        var classTmp; // an array to temporary store the class of each element
        Allchilds.forEach(child => { // for each childs
            classTmp = []; // I set the array to its beginnig value
            child.className.split(" ").forEach(cl => { //for each class of the child
                if (cl!=classN){ // if the class of the child is different
                    classTmp += " "+cl;// add it to the temporary array
                }//otherwise do nothing
            });
            child.className = classTmp; // set up all the class except the one specified of the child
        });
    }

    getOneTag(idParent, tag){
        // idParent -> the id of the container
        // tag -> a tag name
        // return -> an array
        // goal -> return an array of html nodes, and the tag of the 
        // html node is "tag"
        var arrayChild = this.getAll(idParent); // we took all the child of "idParent"
        var array = []; // we create a temporary array
        arrayChild.forEach(oneChild => { // for each child
            if (oneChild.tagName==tag){ // if the tag of the child correspond to "tag"
                array = array.concat(oneChild); // we add the child to the temporary array to return them
            }
        });
        return array;
    }
    getClass(idParent ,classN ,tag ){
        // idparent -> id of the parent container
        // classN -> class you want in your return array
        // tag -> the tag you want in your return array
        // warning : the items have to contains only one time the specified class
        //return all the childs of idparents, with the class "classN" and the tag "tag"
        var array = [];
        this.getOneTag(idParent,tag).forEach(element => { // for each child of idParent, wich contain the tag tag
            element.className.split(" ").forEach(elementC => {
                if (elementC==classN){ // check in their class if the element is found
                    array = array.concat(element); // add the item to the item
                }
            });
        });
        return array;//renvoi une array des child contenant comme class chaine et que de type tag
    }

    soust(tab1,tab2){
        //tab1, tab2 -> array
        // return the substraction of tab 1 and tab 2 
        var test;
        var array = [];
        tab1.forEach(i1 => {
            test = true;
            tab2.forEach(i2 => {
                if ((i1==i2)){
                    test=false;
                }
            });
            if (test==true){
                array = array.concat(i1);
            }
        });
        return array;
    }

    setPosition(element,x,y){ 
        //element -> html node
        // x -> vertical position of the element, a number
        // y -> horizontal position of the element, a number
        element.style.top = x+"px";
        element.style.left = y+"px";
    }

    sortWithout(table,classN){
        //table -> an array of html element
        // classN -> String, the class you want to exclude from your array
        // return an array of all the html element in table except the one with the class "ClassN"
        var find;
        var arrayReturn = Array();
        var classeNames = [];
        table.forEach(element => {
            classeNames = element.className.split(" ");
            find=false;
            classeNames.forEach(cl => {
                if (cl==classN){
                    find=true;
                }
            });
            if (find==false){
                arrayReturn = arrayReturn.concat(element);
            }
        });
        return arrayReturn;
    }

    hiddOther(idParent,classN,classHide){ 
        // idParent -> the id of the container
        // classN -> the class of each element we want to see
        // classHide -> the class wich will be added to hide the element selected
        // return -> nothing
        // goal -> add the class classToAdd, to each child of idParent
        // except the one who have the classN
        this.filter(idParent,classHide); // I remove the class classToAdd
        //of each element inside idParent
        var array = this.soust(this.getAll(idParent),this.getClass(idParent,classN,"DIV"));
        // First, I take all the child,
        // Secondly, all the child who have the class
        // and it gave me all the child who do not have the classN in there class
        array.forEach(element => {
            element.className=element.className+" "+classHide;
            // for each child without the class classN, I add them
            // the class classToAdd to hide them 
        }); 
    }

}