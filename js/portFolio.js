class PortFolio extends documentHelper{ 
    
    constructor(){
        super();
        this.attributeSelection = "id";
        this.idContainer = "ContainerPortFolio";
        this.portfolioSection = "portFolio";
        this.idMenu = "menuPortFolio";
        this.classHide = "hidden";
        this.classBtnActive = "activeBtn"
        this.clickType = "click";
        this.attributUrl = "href";
        this.stylePointer = "pointer";
        this.oldWidth = 0;
        this.data = "";
        this.idProject = "";
        PortFolio.myself = this;
    }

    setProject(){
        //idContainer = "containerPortfolio"
        //idMenu = "menuPortFolio"
        //classtoHide = "hidden"
        var array = super.getAllExct(this.idContainer,this.classToHide); // take all the the element of idcontainer except the one with classToHide
        array = super.sortWithout(array,this.classHide);       
        var widthElem = array[0].clientWidth; // set up the max width
        var heigthElem = array[0].clientHeight; // set up the max height
        var portfolio = document.getElementsByClassName(this.portfolioSection).item(0); //took the portfolio parent
        var menuPf = document.getElementById(this.idMenu); 
        var marginVr = 0.2*(menuPf.offsetHeight); // set the size of the vertical margin
        var width = portfolio.clientWidth; // set the total width
        var marginHr = 0.05*width; // set the margin in of the total width
        var find = false; //if the perfect size is found
        var i = 0; // used in order to count the number of column of item
        while (find == false) { // while the perfect size is not found
            if (i*widthElem+2*marginHr+(i-1)*marginHr>width){ // while the whole size of the portfolio is still under the width
                i--; // if the portfolio is larger than the witdh we substract 1 in order to set the width of the portfolio in the whole width
                find = true; // and we told hey we find the perfect width
            }else{
                i++ ;// otherwise we continue to add the number of column
            }
        }
        if (i<1){ // if the width is not large enought we still set up one column
            i=1;
        }
        //positionning of the portfolio
        var center = i*widthElem+(i-1)*marginHr; 
        var sides = (width-center)/2; 
        var left = sides;
        var incre = Math.ceil(array.length/i);
        var top = menuPf.offsetHeight+menuPf.offsetTop;
        var botContainer = top + (marginHr+heigthElem)*incre+incre*marginVr;// ici i doit etre egale a array.length/2 arrodnie à l'entier superieur
        var topContainer = menuPf.offsetHeight+menuPf.offsetTop;
        var heightPF = botContainer - topContainer;
        portfolio.style.paddingBottom = heightPF+"px";
        for (let index = 0; index < array.length; index++) {
            const element = array[index];
            super.setPosition(element,top+i*marginVr,left); // set up all the element of the portfolio at the expected position
            left+=marginHr+widthElem;
            if (((index+1)%i)==0){ //set up the width in of their position
                left = sides;
                top += marginHr+heigthElem;
            }
        }        
    }

    // what happen when you click on a attribute
    onclickAttribute(element){
        super.hiddOther(this.idContainer,element.getAttribute(this.attributeSelection),this.classHide);
        super.filter(this.idMenu,this.classBtnActive);
        // e -> an html node
        // return -> nothing
        // goal -> add the class classN to the clicked button
        var classN = element.className.split(" "); //I take all the class and set them up in an array
        var test=false; // I tell we do not have already find the class 
        var e; // a temporary variable
        for (let index = 0; index < classN.length; index++) { // for each class of the button
            e = classN[index];
            if (e==this.classBtnActive){// I check if i've found the class
                test=true; // i told "i've find it"
                break; // and i stop the loop
            }
        }
        if (!test==true){ // if I've not find the class
            element.className += " "+this.classBtnActive; // i add it to the button
        }
        this.setProject();
    }

    //When we have to redirect with an URL if a HREF attribute is specified
    linkContainer(){
        var array = super.getAll(this.idContainer);
        var url = null;
        array.forEach(element => {
            element.childNodes[1].addEventListener(this.clickType,(e)=>{
                url = e.target.parentNode.getAttribute(this.attributUrl);
                if(url!=null){
                    document.location.href=url;
                }
            });
            url = element.getAttribute(this.attributUrl);
            if(url!=null){
                element.style.cursor = this.stylePointer;
            }
        });
    }

    //-----------------------------------------------------------------Few method to make appear a popup on a project click-----------------------------------------------------

    //A small method used to make disappear the popup
    removePopup(){
        var span = document.getElementById("portfolioSpan");
        span.className = "hidden";
        setTimeout(function(){span.remove()},500);
    }

    //Method wich construct threw a DOM object the popup, and make it apear
    setPopup(dataJson = ""){
                
        if(dataJson!=""){
            PortFolio.myself.data = dataJson;
        }
        if(PortFolio.myself.data[PortFolio.myself.idProject]!=undefined){
            var span = document.createElement("span");
            var div = document.createElement("div");
            var h1 = document.createElement("h1");
            h1.textContent = PortFolio.myself.data[PortFolio.myself.idProject].title;
            div.appendChild(h1);
            var div2 = document.createElement("div");
            var div3 = document.createElement("div");
            var img = document.createElement("img");
            img.src = PortFolio.myself.data[PortFolio.myself.idProject].image[1];
            var divTxt = document.createElement("div");
            divTxt.innerHTML = PortFolio.myself.data[PortFolio.myself.idProject].description[0];
            div3.appendChild(img);
            div3.appendChild(divTxt);
            div2.appendChild(div3);
            var div4 = document.createElement("div");
            var img2 = document.createElement("img");
            img2.src = PortFolio.myself.data[PortFolio.myself.idProject].image[2];
            var divTxt2 = document.createElement("div");
            divTxt2.innerHTML = PortFolio.myself.data[PortFolio.myself.idProject].description[1];
            div4.appendChild(divTxt2);
            div4.appendChild(img2);
            div2.appendChild(div4);
            if(PortFolio.myself.data[PortFolio.myself.idProject].gitLink!==""){
                var a = document.createElement("a");
                a.href = PortFolio.myself.data[PortFolio.myself.idProject].gitLink;
                a.textContent = "Pour en savoir plus";
                div2.appendChild(a);
            }
            div.appendChild(div2);
            span.appendChild(div);
            span.id = "portfolioSpan";
            span.className ="hidden";
            span.firstElementChild.addEventListener("click",PortFolio.myself.removePopup,false);
            document.getElementById("projects").insertBefore(span,document.getElementById("ContainerPortFolio"));
            setTimeout(function(){span.className="active"},1);
        }else{
            alert("fonction not developed yet"); 
        }
    }
    // Function which use data from xhr and load it into setPopup function
    //@param -> besoin de la variable de class "id"
    projectOnclick(id) {
        PortFolio.myself.idProject = id;              
        if(this.data==="") {
            var xhr = new XHR("GET",null);
            xhr.responsetype = "json";
            xhr.callbackLoad = this.setPopup;
            xhr.init();
        }else{
            this.setPopup();
        }
    }

    init(){
        let array = super.getAll(this.idMenu);
        array.forEach(element => {
            element.addEventListener(this.clickType,function(e){
                PortFolio.myself.onclickAttribute(e.currentTarget); 
            });
        });
        this.linkContainer();
        this.setProject();
        var the_timer;
        window.addEventListener('resize', function(){
            if(the_timer!=undefined){
                clearTimeout(the_timer);
            }
            if(PortFolio.myself.oldWidth==window.innerWidth && Menu.myself.menuIsDown==false ){
                the_timer = setTimeout(function(){
                    PortFolio.myself.setProject();//due to scope error we have to use "this" threw a static variable
                }, 75);
            }else if(PortFolio.myself.oldWidth!=window.innerWidth){
                PortFolio.myself.oldWidth=window.innerWidth;
                the_timer = setTimeout(function(){
                    PortFolio.myself.setProject();//due to scope error we have to use "this" threw a static variable
                }, 75);
            }
        });
        //init of XHR poppup for project
        var elementPortFolio = document.getElementById("ContainerPortFolio").children;
        for (let i = 0; i < elementPortFolio.length; i++) {
            elementPortFolio.item(i).addEventListener("click",function(e){  
                PortFolio.myself.projectOnclick(elementPortFolio.item(i).id);
            });
        }

    }
}
//---------------------------------------------------------------------------------------------------------------------