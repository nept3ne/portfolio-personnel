class Slide extends documentHelper{

    constructor(){
        super();
        this.classParent = "slides"; // class of the element with all the portfolio element inside of him
        this.classActiveR = "activeR"; // class to set when it is active
        this.classActiveL = "activeL";
        this.tagLinks = "A"; // the tag of button
        this.rightClass = "rightArrow"; // class of the right button
        this.leftClass = "leftArrow"; // class of the left button
        this.pattern = "portfolio"; // what will look like the id of the portfolio
        this.arrayElementRight = document.getElementsByClassName(this.rightClass); //right arrow
        this.arrayElementLeft = document.getElementsByClassName(this.leftClass); //left arrow
        this.idSlides = this.searchSlide(this.classParent,this.pattern); // it is a list of id of each portfolio in the page
        this.index = 0; // increment variable
        this.time = 7000; // time between each automatic turn
        this.intervalId = setInterval(this.tmpnext,this.time); // interval handler
        this.eventType = "click"; // The type of event wich allowed to slide
        this.stopSlide = false;
        Slide.myself = this;    
        this.idSlides.forEach(element => { //loop for each detected nodes
            this.arrayElementRight.item(this.index).addEventListener(this.eventType,function(){
                Slide.myself.next(element);
                if(Slide.myself.stopSlide===false){
                    clearInterval(Slide.myself.intervalId);              // to avoid the activation of the slides just after 
                    Slide.myself.intervalId = setInterval(Slide.myself.tmpnext, Slide.myself.time);// pressing the button
                }
            });
            this.arrayElementLeft.item(this.index).addEventListener(this.eventType,function(){
                Slide.myself.previous(element);
                if(Slide.myself.stopSlide===false){
                    clearInterval(Slide.myself.intervalId);              // to avoid the activation of the slides just after
                    Slide.myself.intervalId = setInterval(Slide.myself.tmpnext, Slide.myself.time);// pressing the button
                }
            });
            this.index = this.index+1;
        });
    }

    getNext(idParent){        
        //idParent -> the id of the container
        // it's goal : 
        // one of its child have a class named "active", and we want to return 
        // the element just after this class 
        const Slides = super.getAllExct(idParent, this.tagLinks); // we took all type of element except the A (which are links)
        
        let next;
        let element;
        Slides.forEach(item => {
            if(item.className==this.classActiveL){
                element = item;
            }else if(item.className==this.classActiveR){
                element = item;
            }
        });
        if(element!=undefined){          
            next = element.nextElementSibling;
            if(next.tagName===this.tagLinks){
                next = Slides[0];
            }
            element.className = "fromActiveToHidL";
        }else{
            next = Slides[0]
        }
        
        setTimeout(function () {
            if((element.className!==slide.classActiveR && element.className!==slide.classActiveL ))
            {
                element.className = "";
            }
        },1000);


        return next; // return the one find
    }

    getPrevious(idParent){
        // idParent -> the id of the container
        // it's goal : 
        // one of its child have a class named "active", and we want to return 
        // the element just before this class 
        // if not it will return the first element
        const Slides = this.getAllExct(idParent, this.tagLinks); //we take every element except the links
        let previous;
        let element;
        Slides.forEach(item => {
            if(item.className==this.classActiveL){
                element = item;
            }else if(item.className==this.classActiveR){
                element = item;
            }
        });
        if(element!=undefined){
            previous = element.previousElementSibling;
            if(previous.tagName==this.tagLinks){
                previous = Slides[Slides.length-1];
            }
            element.className = "fromActiveToHidR";
        }else{
            previous = Slides[0];
        }
        
        setTimeout(function () {
            if((element.className!==slide.classActiveR && element.className!==slide.classActiveL ))
            {
                element.className = "";
            }

        },1000);


        return previous;
    }

    next(idParent){
        //function wich will erase each class active, and set the new active class to the selected item
        const element = this.getNext(idParent); // store the element after the one which contains the class "active"
        super.filter(idParent,this.classActiveR); // it remove from all the child of "slides", the class "active"
        super.filter(idParent,this.classActiveL); // it remove from all the child of "slides", the class "active"
        element.className = this.classActiveL; // it add to the next element, the class "active"
    }

    previous(idParent){
        //function wich will erase each class active, and set the new active class to the selected item
        const element = this.getPrevious(idParent); // store the element before the one which contains the class "active"
        super.filter(idParent,this.classActiveL); // it remove from all the child of "slides", the class "active"
        super.filter(idParent,this.classActiveR); // it remove from all the child of "slides", the class "active"
        element.className = this.classActiveR; // it add to the previous element, the class "active"
    }

    searchSlide(){
        //classN -> is the class wich determine if the element is a portfolio or not
        //pattern -> use to set up a generic id for the portfolio
        //Its goal : 
        //search every item wich contain the class classN and add to them an id wich is the pattern and a number at the end
        let portfolio = [];
        const allElement = document.getElementsByClassName(this.classParent);
        for (let i = 0; i < allElement.length; i++) {
            allElement[i].id = this.pattern+i;
            portfolio.push(this.pattern+i); 
        }
        return portfolio;
    }

    tmpnext(){
        //what is needed : 
        //idSlides -> each portfolio in the page
        //classActive -> the name of the class when it is active
        //taglinks -> the tag of button
        //what it does :
        //a basic function wich call an other one
        //every x seconds it turn the slides for each slides
        Slide.myself.idSlides.forEach(element => { //loop for each detected nodes
            this.next(element);
            this.index = Slide.myself.index+1;
        });
    }
    stop(){
        this.stopSlide = true;
        clearInterval(this.intervalId);
        return this;
    }
    resume(){
        this.stopSlide = false;
        this.intervalId = setInterval(this.tmpnext,this.time);
        return this;
    }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}


/*
/////////////////////////////////////// Idea ///////////////////////////////////////////////// 
        - set a button to stop the timer
        - use the keyboard to change the slides :
            if a key is pressed
                if a slide is in the screen
                    we load the next or the previous in function of the key pressed (lazyload)
        - add a function wich set up in first/last the left/right arrow
        - automaticly add the right/left function if the aren't there (at the first last child)

/////////////////////////////////////// Typical structure ////////////////////////////////////
        !!!!!!        
        The parent need the "slides" classes
        It need one son with "leftArrow" classes in first position
        It need one son with "rightArrow" classes in last position
        The "active" class is the first image or text that will appear
        !!!!!!
        <html>
            <div class="slides">
                <a class="leftArrow"></a>
                <img src="an image" class="active" heigth="300px">
                <div>
                    <div>
                        <h3>A little bit of lorem ipsum</h3>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque blandit vitae nunc sed sagittis. Nam
                        volutpat eu lectus id varius. Sed molestie nulla accumsan, scelerisque ligula id, consectetur arcu</p>
                </div>
                <img src="an image" height="300px">
                <a class="rightArrow"></a>
            </div>
        </html>
        
*/