class XHR{
    static IpServer = window.location.host;
    
    constructor(method="GET",parameter="",folder = "js/data.json"){
        this.method = method;
        this.parameter = parameter;
        this.responsetype = "text";
        this.callbackLoad = (e) =>{
            alert("done");
        };
        this.callbackError = function(){
            alert("an error occured");
        };
        this.async = true;
        this.requestURL = 'http://'+XHR.IpServer+"/"+folder;
        XHR.myself = this;
        
    }
    setLoading(){
        var span = document.createElement("span");
        span.className = "loading";
        span.appendChild(document.createElement("span"));
        span.appendChild(document.createElement("span"));
        span.appendChild(document.createElement("span"));
        document.body.appendChild(span);
    }
    removeLoading(){
        document.getElementsByClassName("loading").item(0).remove();
    }
    init() {
        var request = new XMLHttpRequest();
        if(this.method==="GET"){
            this.requestURL +="?";
            this.requestURL +=this.parameter;
            this.parameter = null;            
        }

        
        
        
        request.open(this.method, this.requestURL, this.async);
        request.onloadstart = this.setLoading;
        request.onloadend = this.removeLoading;
        request.onload = function(){
            if(this.responsetype=="text"){
                XHR.myself.callbackLoad(request.responseText);
            }else if(this.responsetype =="json"){
                console.log(JSON.parse(request.response));
                
                XHR.myself.callbackLoad(JSON.parse(request.response));
            }else{
                XHR.myself.callbackLoad(request.response);
            }
        };
        
        
        request.onerror = this.callbackError;
        request.responsetype = this.responsetype;
        request.send(this.parameter);        
    }

}