//TODO faire le changement en class

class Menu{

    constructor(){
        this.menuIsDown = false; //we say the menu is not down
        this.isclear = false;
        this.btnNav = document.getElementById('navigationButton'); //I get the button to show or hide the menu
        this.btnNav.addEventListener("click",this.swipeState); // I told to the computer hey, listen to the navigation button and if so. click on it, execute the piece of code above
        this.navId = document.getElementById('nav'); // I take the id "nav"
        this.navTag = document.getElementsByTagName('nav').item(0) // i take the tag "nav"
        this.Menu = nav.childNodes; // The Menu item displayed
        //It take the container of the middle section
        this.Menu.forEach(element => {
            if(element.tagName=="UL"){
                this.btn = element; 
            }
        });
        this.btn = this.btn.childNodes; // this.btn are the button clickable in the middle section, included some unwanted textNodes
        this.btn.forEach(element=>{
            if(element.tagName!=null){ // we sort if the element is a proper button or not
                element.addEventListener("click",this.swipeState);
            }
        })        
        Menu.myself = this;
    }

    //A small function wich "show", the menu in order to make him displayed
    clear(){
        var arrayTemp = " ";
        Menu.myself.navId.className.split(" ").forEach(element =>{
            if(element != "hide"){
                arrayTemp += " "+element.toString();
            }
        });
        Menu.myself.navId.className = arrayTemp;
        Menu.myself.isclear = true;
    }

    //A method, wich change the state of the menu, open or closed
    swipeState(){
        if(Menu.myself.isclear == false){
            Menu.myself.clear();
        }
        var btn = document.getElementById('navigationButton'); //I get the button to show or hide the menu
        if (Menu.myself.menuIsDown==false){ // if the menu is not down
            Menu.myself.navTag.className="navActive";   // I active the menu
            btn.className="navBtnActive"; // I also change the button
            document.documentElement.style.overflow = 'hidden'; // I stop the scroll
            Menu.myself.menuIsDown=true; // I told the menu is down
            PortFolio.myself.setProject();
        }
        else{ 
            Menu.myself.navTag.className=""; // I set up the menu to it's initial position
            btn.className=""; // I set up the button to it's initial position
            document.documentElement.style.overflow = 'initial'; // I allowed the user to scroll
            Menu.myself.menuIsDown=false; // I told the menu is down
            PortFolio.myself.setProject();// need to atcualize the portfolio, due to the fact that I stop actualizing the portfolio while the menu is down
        }
    }
}