var timeoutActualisation = null;
document.addEventListener('keydown', (event) => { // in case of escape key
    const key = event.key;
    if(event.repeat){
        return null;
    }

    let height;
    let offsetHeight;

    switch (key) {
        case 'Escape':
            changeMenu();
            break;
        case 'ArrowRight':
            //not working
            //TODO Rigth and Left arrow keys
            height = window.innerHeight; // hauteur de l'ecran
            offsetHeight = window.pageYOffset; // ou se situe l'ecran par rapport au haut de la page
            Slide.myself.idSlides.forEach(element => {
                element = document.getElementById(element);
                if(element.offsetTop<height+offsetHeight && element.offsetTop+element.clientHeight>offsetHeight){
                    Slide.myself.next(element.id);
                }
                
            });
            break;
        case 'ArrowLeft':
            height = window.innerHeight; // hauteur de l'ecran
            offsetHeight = window.pageYOffset; // ou se situe l'ecran par rapport au haut de la page
            Slide.myself.idSlides.forEach(element => {
                element = document.getElementById(element);
                if(element.offsetTop<height+offsetHeight && element.offsetTop+element.clientHeight>offsetHeight){
                    Slide.myself.previous(element.id);
                }

            });
            break;
        default:
            //do nothing
            break;
    }
}, false);