<?php
    function actualiseJSON($fileName,$idTarget,$newId,$jsonStringProject ){
        $parsedStringJson = json_decode($jsonStringProject, true);
        $json = file_get_contents($fileName); 
        $parsed_JSON = json_decode($json, true); 
        if($idTarget!=null){ 
            unset($parsed_JSON[$idTarget]);
        }
        $parsed_JSON[$newId] = $parsedStringJson;
        $json = json_encode($parsed_JSON);  
        file_put_contents($fileName, $json); 
    }
    function deleteJSON($fileName,$id){
        $json = file_get_contents($fileName);
        $parsed_JSON = json_decode($json, true);
        unset($parsed_JSON[$id]);
        $json = json_encode($parsed_JSON);
        file_put_contents($fileName, $json);
    }
    if(!isset($_GET["delete"])){
        actualiseJSON("../js/data.json",$_GET["id"],$_GET["id"],$_GET["param"]);
    }else{
        deleteJSON("../js/data.json",$_GET["id"]);
    }
    /*
        l'objet passe en parametre est de type : mais en stringify

                "title": "BizzBee project",
                "image": [
                    "./img/P9-Bizzbee/first.jpg",
                    "./img/P9-Bizzbee/second.jpg",
                    "./img/P9-Bizzbee/third.jpg"
                ],
                "description": [
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam vitae justo velit. Morbi eu leo ac dolor sodales pulvinar in convallis odio. Nam consectetur dui arcu. Nunc scelerisque urna sit amet purus scelerisque, vitae dapibus ligula malesuada. Mauris erat ipsum, placerat eget sem eget, placerat suscipit leo. Sed laoreet efficitur eros eget mollis. Nam at convallis leo. Etiam congue pretium nibh, vel tincidunt tellus semper eu. Nunc scelerisque auctor tellus ac rutrum. Phasellus ex lorem, rhoncus sed maximus eu, luctus nec risus. Vivamus maximus diam eu leo ullamcorper, a malesuada massa suscipit. Aliquam erat volutpat. Etiam ultrices dignissim lectus, eget lobortis lacus egestas nec. Duis et velit velit. Donec tincidunt ac orci eu rutrum.",
                    "Vivamus congue orci metus, viverra facilisis nulla luctus eget. Nunc porttitor risus ac felis mollis, rutrum euismod augue porta. Nullam orci magna, ultrices eget eros in, hendrerit egestas eros. Fusce vulputate et dolor id laoreet. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aliquam euismod est quis arcu lobortis sollicitudin. Fusce sit amet posuere quam."
                ],
                "gitLink": "https://gitlab.com/guillemi"
    */
?>