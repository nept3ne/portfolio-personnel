const minify = require('@node-minify/core');
const uglifyES = require('@node-minify/uglify-es');
// const htmlMinifier = require('@node-minify/html-minifier');

minify({
    compressor: uglifyES,
    input: '../js/functionOth.js',
    output: '../js/functionOth.min.js',
    callback: function (err, min) {}
});
minify({
    compressor: uglifyES,
    input: '../js/menu.js',
    output: '../js/menu.min.js',
    callback: function (err, min) {}
});
minify({
    compressor: uglifyES,
    input: '../js/portFolio.js',
    output: '../js/portFolio.min.js',
    callback: function (err, min) {}
});
minify({
    compressor: uglifyES,
    input: '../js/slide.js',
    output: '../js/slide.min.js',
    callback: function (err, min) {}
});
minify({
    compressor: uglifyES,
    input: '../js/keyboardEvent.js',
    output: '../js/keyboardEvent.min.js',
    callback: function (err, min) {}
});

// minify({
//     compressor: htmlMinifier,
//     input: '../index.html.html',
//     output: '../index.min.html',
//     callback: function(err, min) {}
//   });