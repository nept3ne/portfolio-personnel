<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Modification of project</title>
</head>
<body>
<main>
    <label for="project-selection">Select Your project</label>
    <select name="projects" id="project-selection">
        <!--ajout en js des options-->
        <option value=""></option>
    </select>
    <div id="id">
            <p>id</p>
            <input type="text" class="id">
        </div>
    <div>
        <p>title</p>
        <input type="text" class="title">
    </div>
    <div>
        <p>image 1</p>
        <input type="url" class="image">
    </div>
    <div>
        <p>image 2</p>
        <input type="url" class="image">
    </div>
    <div>
        <p>image 3</p>
        <input type="url" class="image">
    </div>
    <div>
        <p>description 1</p>
        <textarea class="description"></textarea>
    </div>
    <div>
        <p>description 2</p>
        <textarea class="description"></textarea>
    </div>
    <div>
        <p>description 3</p>
        <textarea class="description"></textarea>
    </div>
    <div>
        <p>gitlink</p>
        <input type="url" name="" id="" class="gitlink">
    </div>
    <a id="send">send</a>
    <a id="delete">delete</a>
</main>
</body>
<script src="../js/xhr.js"></script>
<script>
    /*function xhr(id,pakage,deleteJSON = null){
        var requestURL = <?php echo "'http://".$_SERVER["SERVER_ADDR"]."/SitePerso/script/ModifJson.php'";?>;// TODO dans l'idéal le faire en PHP via la variable SERVEUR
        var request = new XMLHttpRequest();

        pakage = "id="+id+"&param="+pakage;
        if(deleteJSON == true){
            pakage += "&delete=true";
        }else{

        }
        request.open('POST', requestURL, true);
        request.setRequestHeader('Access-Control-Allow-Origin','*');
        request.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
        request.onerror = function () {
            alert("Sorry, an error occured please try later or inform the admin");
        }
        request.onload = function () {
                    alert("Everithing went well");
                    updateData(id);
                }
        request.send(pakage);
    }*/



    document.getElementById("send").addEventListener("click",function(e){
        if(document.getElementsByClassName("id").length==1){
            id = document.getElementsByClassName("id").item(0).value
            document.getElementById("id").remove();
        }else{
            id = document.getElementById("project-selection").value;
        }
        if(id!=""){
            let project = new Object();
            project.title = document.getElementsByClassName("title").item(0).value;
            project.image = new Array();
            project.description = new Array();

            images = document.getElementsByClassName("image");
            for (let i = 0; i < images.length; i++) {
                if(images.item(i).value!="") {
                    project.image[i] = images.item(i).value;
                }
            }
            descriptions = document.getElementsByClassName("description");
            for (let i = 0; i < descriptions.length; i++) {
                if(descriptions.item(i).value!="") {
                    project.description[i] = descriptions.item(i).value;
                }
            }
            project.gitLink = document.getElementsByClassName("gitlink").item(0).value;
            console.log(project);
            
            xhr = new XHR("GET","id="+id+"&param="+JSON.stringify(project),"SitePerso/script/ModifJson.php" );
            xhr.init();
           
        }else{
            alert("la requete doit au moins comporter un ID");
        }
    });

//---------------------------------Add--------------------------------------

    /**
     * Onload function, used when the xhr request is done 
     */
    function XHRonload(reponse){
        e = event;
        request = this;
        if(data!=""){
            data = reponse;
        }
        
        select = document.createElement("select");
        select.name = "projects";
        select.id = "project-selection";
        option = document.createElement("option");
        option.innerHTML = "-- veuillez choisir un projet --";
        option.value = "none";
        select.appendChild(option);
        for(const element in data){            
            option = document.createElement("option");
            option.value=element;
            if(e==element){
                option.selected = true;
            }
            option.innerHTML = element;
            select.appendChild(option);
        }
        select.addEventListener("input",listenerChange);
        document.getElementById("project-selection").parentNode.replaceChild(select,document.getElementById("project-selection"));

    }
    
    /**
    * This function update the data var, in the case that the content of the file data.json has changed, it also update all the field blank field, if a value is selected
    * @param {id} e - The id of the value selected on "SELECT" item
    */
    function updateData(e=null){
        event = e;
        xhr = new XHR();
        xhr.callbackLoad = XHRonload;
        xhr.responsetype = 'application/json';
        xhr.init();

    }


    /**
     *
     */
    function listenerChange(e){
        updateData(e.target.value);
        if(data[document.getElementById("project-selection").value]!=null ){
            //case if the item choosed is recognize in the data
            fillData(e);
        }else{
            //case if the item choosed is not recognize in the data
            fillBlank()
        }
    }
    /**
     * Select every field (except "select") and fill them with data
     * @param {Object} data - The `Object` that contains the data to fill the blank with
     * @param {event} e - The event of throwed by the "select" HTMLElement
     */
    function fillData(e){
        let project = data[document.getElementById("project-selection").value];
        let title = document.getElementsByClassName("title").item(0);
        title.value = project["title"];
        let image = project["image"];
        if(image[0]==undefined){
            for(i=0;i<document.getElementsByClassName("image").length;i++){
                document.getElementsByClassName("image").item(i).value="";
            }
        }else{
            for(i=0;i<image.length;i++){
                document.getElementsByClassName("image").item(i).value = image[i];
            }
        }
        let description = "";

        description = project["description"];
        if(description[0]==undefined){
            for(i=0;i<document.getElementsByClassName("description").length;i++){
                document.getElementsByClassName("description").item(i).value="";
            }
        }else{
            for(i=0;i<description.length;i++){
                document.getElementsByClassName("description").item(i).value = description[i];
            }
        }
        document.getElementsByClassName("gitlink").item(0).value = project["gitLink"];
        if(document.getElementById("id")!=null){
            document.getElementById("id").remove();
        }
    }

    /**
     * Select every input (except "select") and fill them with blank
     */
    function fillBlank(){

        let title = document.getElementsByClassName("title").item(0);
        title.value = "";

        let image = document.getElementsByClassName("image")
        for(i=0;i<image.length;i++){
            document.getElementsByClassName("image").item(i).value = "";
        }

        let description = document.getElementsByClassName("description")
        for(i=0;i<description.length;i++){
            document.getElementsByClassName("description").item(i).value = "";
        }

        document.getElementsByClassName("gitlink").item(0).value = "";

        if(document.getElementById("id")==null){ //to avoid multiple Id
            let div = document.createElement("div");
            div.id = "id";
            div.innerHTML = '<p>id</p><input type="text" class="id">';
            document.getElementById("project-selection").parentNode.insertBefore(div,document.getElementById("project-selection").nextElementSibling)
        }
        updateData();
    }


//---------------------------------Main--------------------------------------
    var data; // a global var wich will be used all along the script
    var event; // a global var in order to handle event

    updateData();

    document.getElementById("project-selection").addEventListener("input",listenerChange);



</script>
<script>
//---------------------------------Delete--------------------------------------
    document.getElementById("delete").addEventListener("click",function(e){
        id = document.getElementById("project-selection").value;
        if(id!=""){
            xhr = new XHR("GET","id="+id+'&delete=true',"SitePerso/script/ModifJson.php");
            xhr.async = false;
            xhr.init();
            child = document.getElementById("project-selection").children;
            for(let i = 0;i<child.length;i++){
                if(child.value==id){
                    child.remove();
                }
            }
        }
        fillBlank();
    });
</script>
<style>
    main{
        display : flex;
        flex-direction : column ;
        align-items:center;
    }
    label,a{
        margin-top : 10px;
        margin-bottom : 10px;
    }
    div{
        width : 100%;
    }
    input,textarea{
        height : 100%;
        font-family: "Nunito Light";
        font-size: 15px;
        width : 100%;
    }
    a{
        padding : 0.75em;
        background-color : #F05F40;
        border-radius : 100px;
        margin-bottom : 2em;
        color : white;
        font-weight: 800;
        transition: transform 0.5s, background-color 1.5s;
        padding-right : 2.5em;
        padding-left : 2.5em;
    }
</style>
</html>